import os
import inspect
from typing import Callable, Union, List, Dict, Tuple
import pytest
import xml.etree.ElementTree as ET

from azure.devops.connection import Connection
from msrest.authentication import BasicAuthentication
from azure.devops.v5_1.test.models import RunCreateModel, ShallowReference


testParams = List[
    Dict[str, str]
]


@pytest.fixture(scope="module")
def global_state():
    return dict()


class Tester:
    def __init__(self, orgUrl: str = None, project: Union[int, str] = None,
                 PAT: str = None):
        self.credentials = BasicAuthentication('', PAT)
        self.orgUrl = orgUrl
        self.project = project

    @property
    def connection(self):
        print("AAAAAAAAAAAAAAAAAAAAAA")
        if not hasattr(self, "__connection"):
            self.__connection = Connection(base_url=self.orgUrl,
                                           creds=self.credentials)
        return self.__connection

    @property
    def testClient(self):
        if not hasattr(self, "__testClient"):
            self.__testClient = self.connection.clients\
                                .get_test_client()
        return self.__testClient

    @property
    def testplanClient(self):
        if not hasattr(self, "__testplanClient"):
            self.__testplanClient = self.connection.clients\
                                    .get_test_plan_client()
        return self.__testplanClient

    @property
    def witClient(self):
        if not hasattr(self, "__witClient"):
            self.__witClient = self.connection.clients\
                               .get_work_item_tracking_client()
        return self.__witClient

    @staticmethod
    def parse_parameters(xml_string) -> testParams:
        result = []
        root = ET.fromstring(xml_string)
        for row in root[1:]:
            parameter = {}
            for element in row:
                parameter[element.tag] = element.text
            result.append(parameter)
        return result

    @staticmethod
    def map_parameters_names(parameters: testParams,
                             parameterNameMapping: Dict[str, str]):
        newParameters = []
        for row in parameters:
            newRow = {}
            for name, value in row.items():
                if name in parameterNameMapping:
                    newName = parameterNameMapping[name]
                    newRow[newName] = value
                else:
                    newRow[name] = value
            newParameters.append(newRow)
        return newParameters

    @staticmethod
    def map_parameters_values(parameters: testParams,
                              parameterValueMapping: Dict[str, str]):
        newParameters = []
        for row in parameters:
            newRow = {}
            for name, value in row.items():
                if value in parameterValueMapping:
                    newValue = parameterValueMapping[value]
                    newRow[name] = newValue
                else:
                    newRow[name] = value
            newParameters.append(newRow)
        return newParameters

    def testcase(self,
                 argname: str,
                 testcaseId: int,
                 parameterNameMapping: Dict[str, str] = None,
                 parameterValueMapping: Dict[str, str] = None):

        global_state = {}

        def decorator(func: Callable):
            if inspect.iscoroutinefunction(func):
                async def newFunc(record_property, *args, **kwargs):
                    
                    counter_key = f"{testcaseId}_counter"
                    if counter_key not in global_state:
                        global_state[counter_key] = global_state['test_num']
                    global_state[counter_key] -= 1

                    record_property("client", self.testClient)
                    record_property("project", self.project)
                    record_property("testcaseId", testcaseId)
                    record_property("testplanId", global_state["testplanId"])
                    record_property("testsuiteId", global_state["testsuiteId"])
                    record_property("testpointId", global_state["testpointId"])
                    record_property("global_state", global_state)

                    record_property(
                        "first_in_group",
                        global_state[counter_key] == global_state['test_num'] - 1
                        )
                    record_property(
                        "last_in_group",
                        global_state[counter_key] == 0
                        )

                    call_args = inspect.getcallargs(func, *args, **kwargs)
                    return await func(**call_args)
            else:
                def newFunc(record_property, meta_parameters, *args, **kwargs):

                    # counter_key = f"{testcaseId}_counter"
                    # if counter_key not in global_state:
                    #     global_state[counter_key] = len(parameters)
                    # global_state[counter_key] -= 1

                    record_property("client", self.testClient)
                    # record_property("project", self.project)
                    # record_property("testplanId", testplanId)
                    # record_property("testsuiteId", testsuiteId)
                    record_property("testcaseId", testcaseId)
                    # record_property("testpointId", testpointId)
                    # record_property("state", global_state)
                    for name, value in meta_parameters.items():
                        record_property(name, value)

                    # record_property(
                    #     "first_in_group",
                    #     global_state[counter_key] == len(parameters) - 1
                    #     )
                    # record_property(
                    #     "last_in_group",
                    #     global_state[counter_key] == 0
                    #     )

                    call_args = inspect.getcallargs(func, *args, **kwargs)
                    return func(**call_args)

            signature = inspect.signature(func)
            signature = signature.replace(parameters=(
                [
                    inspect.Parameter('record_property',
                                      inspect.Parameter.POSITIONAL_OR_KEYWORD),
                ] +
                list(signature.parameters.values())
            ))

            newFunc.__signature__ = signature
            newFunc = pytest.mark.azure_testplans(
                testcaseId=testcaseId,
                tester=self,
                argname=argname,
                parameterNameMapping=parameterNameMapping,
                parameterValueMapping=parameterValueMapping,
                global_state=global_state,
            )(newFunc)
            return newFunc

        return decorator
