import pytest
import inspect
from typing import Callable, Union, List, Dict, Tuple
import xml.etree.ElementTree as ET

from azure.devops.connection import Connection
from msrest.authentication import BasicAuthentication


# testParams = List[
#     Dict[str, str]
# ]


# class Tester:
#     def __init__(self, orgUrl: str, project: Union[int, str], PAT: str):
#         credentials = BasicAuthentication('', PAT)
#         connection = Connection(base_url=orgUrl, creds=credentials)
#         self.testClient = connection.clients.get_test_client()
#         self.witClient = connection.clients.get_work_item_tracking_client()
#         self.project = project

#     @staticmethod
#     def __parse_parameters(xml_string) -> testParams:
#         result = []
#         root = ET.fromstring(xml_string)
#         for row in root[1:]:
#             parameter = {}
#             for element in row:
#                 parameter[element.tag] = element.text
#             result.append(parameter)
#         return result

#     @staticmethod
#     def __map_parameters_names(parameters: testParams,
#                                parameterNameMapping: Dict[str, str]):
#         newParameters = []
#         for row in parameters:
#             newRow = {}
#             for name, value in row.items():
#                 if name in parameterNameMapping:
#                     newName = parameterNameMapping[name]
#                     newRow[newName] = value
#                 else:
#                     newRow[name] = value
#             newParameters.append(newRow)
#         return newParameters

#     @staticmethod
#     def __map_parameters_values(parameters: testParams,
#                                 parameterValueMapping: Dict[str, str]):
#         newParameters = []
#         for row in parameters:
#             newRow = {}
#             for name, value in row.items():
#                 if value in parameterValueMapping:
#                     newValue = parameterValueMapping[value]
#                     newRow[name] = newValue
#                 else:
#                     newRow[name] = value
#             newParameters.append(newRow)
#         return newParameters

#     def testcase(self,
#                  argname: str,
#                  testCaseId: int,
#                  parameterNameMapping: Dict[str, str] = None,
#                  parameterValueMapping: Dict[str, str] = None):
#         testCaseWit = self.witClient.get_work_item(testCaseId)
#         parameters_xml_string = testCaseWit.fields[
#             "Microsoft.VSTS.TCM.LocalDataSource"]
#         parameters = self.__parse_parameters(parameters_xml_string)
#         if parameterNameMapping is not None:
#             parameters = self.__map_parameters_names(parameters,
#                                                      parameterNameMapping)
#         if parameterValueMapping is not None:
#             parameters = self.__map_parameters_values(parameters,
#                                                       parameterValueMapping)

#         def decorator(func: Callable):
#             if inspect.iscoroutinefunction(func):
#                 async def newFunc(*args, **kwargs):
#                     call_args = inspect.getcallargs(func, *args, **kwargs)
#                     return await func(**call_args)
#             else:
#                 def newFunc(*args, **kwargs):
#                     call_args = inspect.getcallargs(func, *args, **kwargs)
#                     return func(**call_args)

#             newFunc.__signature__ = inspect.signature(func)
#             newFunc = pytest.mark.parametrize(argname, parameters)(func)
#             newFunc = pytest.mark.testcase(newFunc)
#             return newFunc

#         return decorator
