from _pytest.mark.structures import Mark, MarkDecorator
import pytest
from azure.devops.v5_1.test.models import \
    TestCaseResult, ShallowReference, RunCreateModel, RunUpdateModel
import logging


def pytest_configure(config):
    config.addinivalue_line(
        "markers", "azure_testplans"
    )


def pytest_runtest_logreport(report):

    properties = dict(report.user_properties)

    if report.when == 'call':
        if 'azure_testplans' in report.keywords:
            testcaseId = properties['testcaseId']
            testClient = properties['client']
            global_state = properties['global_state']

            testrun_key = f'{testcaseId}_testrun'
            if properties['first_in_group']:
                testRun = testClient.create_test_run(
                    RunCreateModel(name="test-run", plan={
                        'id': properties['testplanId']
                        }),
                    properties['project']
                )
                global_state[testrun_key] = testRun
            else:
                testRun = global_state[testrun_key]

            result = TestCaseResult(
                outcome=report.outcome,
                test_point=ShallowReference(id=properties['testpointId']),
            )
            testClient.add_test_results_to_test_run(
                [result],
                properties['project'],
                testRun.id,
            )

            if properties['last_in_group']:
                testClient.update_test_run(
                    RunUpdateModel(state="completed"),
                    properties['project'],
                    testRun.id,
                )


def setup_test_run(testcaseId, tester,
                   parameterNameMapping, parameterValueMapping):
    testcaseWit = tester.witClient.get_work_item(testcaseId)
    parameters_xml_string = testcaseWit.fields[
        "Microsoft.VSTS.TCM.LocalDataSource"]
    parameters = tester.parse_parameters(parameters_xml_string)
    if parameterNameMapping is not None:
        parameters = tester.map_parameters_names(parameters,
                                                 parameterNameMapping)
    if parameterValueMapping is not None:
        parameters = tester.map_parameters_values(parameters,
                                                  parameterValueMapping)
    suite = tester.testplanClient.get_suites_by_test_case_id(testcaseId)[0]
    testsuiteId = suite.id
    testplanId = suite.plan.id
    testpointId = tester.testClient.get_points(
        tester.project, testplanId, testsuiteId, test_case_id=testcaseId
        )[0].id

    meta = {
        "testsuiteId": testsuiteId,
        "testplanId": testplanId,
        "testpointId": testpointId,
        "test_num": len(parameters),
    }

    return parameters, meta


def pytest_addoption(parser):
    parser.addoption("--azure-testplans", action="store_true",
                     help="run azure testplans")


def pytest_generate_tests(metafunc):
    marks = [mark for mark in metafunc.function.pytestmark
             if mark.name == "azure_testplans"]
    if len(marks) == 0:
        return
    mark = marks[0]
    params = mark.kwargs

    is_run = metafunc.config.getoption("azure_testplans")
    if not is_run:
        metafunc.parametrize(params['argname'], pytest.skip())
        return

    parameters, meta = setup_test_run(
        params['testcaseId'],
        params['tester'],
        params['parameterNameMapping'],
        params['parameterValueMapping'],
    )
    metafunc.parametrize(params['argname'], parameters)
    params['global_state'].update(meta)
