from setuptools import setup

setup(
    name="pytest-azure-test-plans",
    entry_points={'pytest11': ['azure-test-plans = pytest_azure_test_plans.plugin']},
    author="akkyma232",
    author_email='akkyma232@gmail.com',
    url='https://gitlab.com/akkyma232/pytest-azure-testplans',
    packages=['pytest_azure_test_plans'],
    install_requires=[
        "pytest",
        "azure-devops",
    ],
    version="0.1"
)
